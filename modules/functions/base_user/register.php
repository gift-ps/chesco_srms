<?php
include('server.php');
include('../assets/vars.php')
 ?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title><?php echo "$pageTitle"; ?></title>
  <link rel="stylesheet" href="./style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="wrapper fadeInDown">
  
  <h3>Add new user</h3>

  <div id="formContent">
    <hr>
    <!-- Icon -->
    <div class="fadeIn first">
      <img src="schoollogo.png" id="icon" alt="School logo" width="100" height="100" />
    </div>

    <!-- Login Form -->
    <form method="post" action="register.php">
          <?php include('../assets/errors.php'); ?>
          
          <input type="text" id="login" class="fadeIn second" name="username" placeholder="username" value="<?php echo $username; ?>">

          <input type="text" id="password" class="fadeIn third" name="email" placeholder="email" value="<?php echo $email; ?>">

          <input type="password" id="password" class="fadeIn third" name="password_1" placeholder="password">

          <input type="password" id="password" class="fadeIn third" name="password_2" placeholder="confirm password">

          <select type="text" class="fadeIn third" name="roles" id="roles" required>
            <option mute>Creating an account for:</option>            
            <option value="Nurse">Nurse</option>
            <option value="House Parent">House parent</option>
            <option value="Administrator">Admin</option>
          </select> 


          <input type="submit" class="fadeIn fourth" name="reg_user" value="Register">


    </form>
    <!--
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>
    -->

  </div>
</div>
<!-- partial -->
  
</body>
</html>
