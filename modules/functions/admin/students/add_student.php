<?php 
    require_once('../scripts/student_validation.php');
    require_once('../../../config/admin_server.php');
    $add_side_bar = true;
    include_once('../layouts/head_to_wrapper.php');
    include_once('../layouts/topbar.php');

?>

<style>
    .table-width {
    padding-right: 75px;
    padding-left: 75px;
    margin-right: auto;
    margin-left: auto;
    }
    @media (min-width: 768px) {
    .table-width {
        width: 750px;
    }
    }
    @media (min-width: 992px) {
    .table-width {
        width: 970px;
    }
    }
    @media (min-width: 1200px) {
    .table-width {
        width: 1170px;
    }
    }
</style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-s border-0 rounded-lg mt-1">

                    <div class="card-header"><h5 class="text-center my-2">Add new student</h5></div>
                    <div class="card-body">
                        <form action="#" method="post" onsubmit="return student_validation();" enctype="multipart/form-data">

                            <table class="table" id="" width="100%" cellspacing="9">
                                <!-- <tr> -->
                                    <!-- <td class="text-left">Student Id:</td> Hide ID input since it's now autogenerated-->
                                    <input id="stuId"type="hidden" name="studentId" placeholder="Enter Id">
                                <!-- </tr> -->
                                <tr>
                                    <td>Student Name:</td>
                                    <td class="text-right"><input id="stuName" type="text" name="studentName" placeholder="Enter Name"></td>
                                </tr>
                                <tr>
                                    <td>Student Username:</td>
                                    <td class="text-right"><input type="text" name="username" placeholder="Enter Username"></td>
                                </tr>
                                <tr>
                                    <td>Student Password:</td>
                                    <td class="text-right"><input id="stuPassword"type="password" name="studentPassword" placeholder="Enter Password"></td>
                                </tr>
                                <tr>
                                    <td>Student Phone:</td>
                                    <td class="text-right"><input id="stuPhone"type="text" name="studentPhone" placeholder="Enter Phone Number"></td>
                                </tr>
                                <tr>
                                    <td>Student Email:</td>
                                    <td class="text-right"><input id="stuEmail"type="text" name="studentEmail" placeholder="Enter Email"></td>
                                </tr>
                                <tr>
                                    <td>Gender:</td>
                                    <td class="text-right">
                                    <input id="m" type="radio" name="gender" value="Male" onclick="stuGender = this.value;"> <label for="m"> Male </label> 
                                    <input id="f" type="radio" name="gender" value="Female" onclick="this.value"> <label for="f">Female</label></td>
                                </tr>
                                <tr>
                                    <td>Student DOB:</td>
                                    <td class="text-right">
                                        <input type="text" name="stuDOB" id="date1" alt="date" class="IP_calendar" title="Y-m-d" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Addmission Date:</td>
                                    <td class="text-right">
                                        <input type="text" name="studentAddmissionDate" id="date1" alt="date" class="IP_calendar" title="Y-m-d" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Student Address:</td>
                                    <td class="text-right"><input id="stuAddress" type="text" name="studentAddress" placeholder="Enter Address"></td>
                                </tr>

                                <tr>
                                    <td>Parents:</td>
                                    <td class="text-right">
                                        <select name="parentid" id="parentid">
                                            <?php
                                            $res = mysqli_query($db, "SELECT * FROM parents");
                                            while($row = mysqli_fetch_array($res)) { ?>
                                            <option value="<?php echo $row['id'];?>"> <?php echo $row['mothername']." and ".$row['fathername']." of ID ".$row['id']; ?> </option>
                                        <?php   }     ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Class:</td>
                                    <td class="text-right">
                                        <select name="class_id" id="class_id">
                                            <?php
                                            $res = mysqli_query($db, "SELECT * FROM classes");
                                            while($row = mysqli_fetch_array($res)) { ?>
                                            <option value="<?php echo $row['id'];?>"> <?php echo $row['name']; ?> </option>
                                        <?php }     ?>
                                        </select>                            
                                    </td>
                                </tr>

                                <tr>
                                    <td>Subjects:</td>
                                    <td class="text-right">
                                        <select name="subjects[]" multiple="multiple" id="subj">
                                              <option disabled> Assign subjects to students </option>
                                            <?php
                                              $res = mysqli_query($db, "SELECT * FROM subjects");
                                              while($row = mysqli_fetch_array($res)) { ?>
                                              <option value="<?php echo $row['id'];?>"> <?php echo $row['name']; ?> </option>
                                            <?php   }     ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Student Picture:</td>
                                    <td class="text-right"><input id="file"type="file" name="file"></td>
                                </tr>
                                    <td></td>
                                    <td class="text-right"><input class="btn btn-sm btn-primary " type="submit" name="create_student"value="Submit"></td>
                                
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




<!-- Multi-Select suport -->
    <link rel="stylesheet" href="../../../assets/select_box/vanillaSelectBox.css">
    <script src="../../../assets/select_box/vanillaSelectBox.js"></script>
    <script>
        let mySelect = new vanillaSelectBox("#subj",{
            maxWidth: 500,
            maxHeight: 400,
            minWidth: -1,
            search: true,
            disableSelectAll: true,
            placeHolder: "Assign subjects",
        });
    </script>
<!-- End multi-select support  -->
            

<?php require_once('../layouts/footer_to_end.php'); ?>

