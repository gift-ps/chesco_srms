<?php 
    require_once('../../../config/admin_server.php');   //contains db connection so we good 🤦🏾‍♂️
    $add_side_bar = true;
    include_once('../layouts/head_to_wrapper.php');
    
    include_once('../layouts/topbar.php');

?>
<script src="bundle.min.js"></script>
        <hr/>

<main>
    <div class="container-fluid col-md-11">

            <div class="card mb-4">
                <div class="card-header text-center">
                    <h3>Classes</h3>
                    <div class="text-right text-light">
                        <a class="btn btn-sm btn-success" href="create_class.php">Add <i class="fas fa-plus "></i> </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="4">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Class Teacher</th>
                                    <th>Class Monitor</th>
                                    <!-- <th>Subjects</th> -->
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $sql = "SELECT * FROM classes";
                                    $results= mysqli_query($db,$sql)or die('An error occured: '.mysqli_error($db));

                                    while($row = mysqli_fetch_array($results)){
                                        $class_id = $row['id'];
                                ?>
                                <tr>
                                    <td><a href="view_class.php?id=<?php echo $row['id'];?>"> <?php echo $row['id']; ?> </a></td>
                                    <td> <?php echo $row['name']; ?></td>
                                    <td>
                                    <?php 
                                        $teacher_id = $row['teacher_id'];
                                        $q = "SELECT name, id
                                        FROM teachers
                                        WHERE id = $teacher_id";

                                        $res = mysqli_query($db, $q) or die(mysqli_error($db));

                                        if (mysqli_num_rows($res) > 0){
                                            while($r = mysqli_fetch_assoc($res)){ 
                                                $techer_name = $r['name'];
                                                echo "<p><a  href='../lecturers/view_lecturer.php?id=".$r['id']."'>".$techer_name."</a></p>";
                                            }
                                        }
                                    ?>
                                    </td>
                                    <td>
                                    <?php 
                                        $student_id = $row['monitor_id'];
                                        $q = "SELECT name, id
                                        FROM students
                                        WHERE id = $student_id";

                                        $res = mysqli_query($db, $q) or die(mysqli_error($db));

                                        if (mysqli_num_rows($res) > 0){
                                            while($r = mysqli_fetch_assoc($res)){ 
                                                $student_name = $r['name'];
                                                echo "<p><a  href='../students/view_student.php?id=".$r['id']."'>".$student_name."</a></p>";
                                            }
                                        }
                                    ?>
                                    </td>

                                    <th><div class="btn-group"><a class="btn btn-success btn-sm text-light" href="view_class.php?id=<?php echo $row['id']?>">View</a>
                                            <a class="btn btn-primary btn-sm text-light " href="update_class.php?id=<?php echo $row['id']?>">Edit</a>  
                                            <a class="btn btn-danger btn-sm text-light" href="#">Delete </a>
                                        </div>
                                    </th>
                                </tr>
                                <?php

                                    }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  


            <script>
                $(document).ready(function() {
                    $('#dataTable').DataTable();
                } );
            </script>

            
    </div>
</main>


<?php require_once('../layouts/footer_to_end.php'); ?>
