<?php
	require_once('header.php');
?>

<body>

    <?php 
      error_reporting(0);
      require '../includes/profile_navbar.php'; 
      $ass_id = $_GET['ass_id'];
      $class_id = $_GET['class_id'];
      $subject_id= $_GET['sub_id'];

      $ass_query = $db->query("SELECT * FROM ass_notice WHERE id='$ass_id' ");
      while($row1 = $ass_query->fetch_assoc()){    
        $ass_name = $row1['name'];  
      }
      
    ?>

<div class="row">



      <!-- column starts here -->
        <div class="col s12 m2">
          <div class="card-panel">
          <?php if (isset($_GET['created'])) { ?>
            <div class="card-panel green center">
                 Graded Successfully
            </div>
          <?php }  ?>
            
          </div><br> <br>
        </div>
      <!-- column ends here --> 
      <br>
      <div class="col s12 m8" style="margin-top: 1em;">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#ass">Assignment</a></li>
          <li class="tab col s3"><a href="#submissions">Submmissions</a></li>
          <li class="tab col s3"><a href="#graded">Graded</a></li>
        </ul>
      </div>

      <!-- First Tab - Assignments -->
      <div id="ass" class="col s12 m8" >
        <div class="card-panel blue">
          <span class="white-text"><?php echo $ass_name; ?> </span>
        </div>
          <br>
        <div class="row">
          <table class="responsive-table striped">
            <thead>
              <tr>
                <th data-field="ass_no">Name</th>
                <th class="txt_limit" data-field="q">Queston</th>
                <th data-field="subject">Subject</th>
                <th data-field="class">Class</th>
                <th data-field="file">File</th>
                <th data-field="final_daet">Date Due </th>
                <th data-field="date">Date Created</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $query = $db->query("SELECT * FROM ass_notice WHERE teacher_id='$t_id'
                              && subject_id = '$subject_id' && class_id = '$class_id' ");

                  while($row=$query->fetch_assoc()){
                    $question_id = $row['id'];
                    $ass_name = $row['name'];
                    $question = $row['question'];
                    $subject=$row['subject_id'];
                    $class=$row['class_id'];
                    $file=$row['assFile'];
                    $dueDate=$row['date_due'];
                    $assDate=$row['date'];

                    $file_path = "../files/ass_notice/".$file; /**File location */

                    $sub_query2 = $db->query("SELECT * FROM classes WHERE id='$class' ");
                    while($row=$sub_query2->fetch_assoc()){    
                      $class_name=$row['name'];  
                    } 
                    $sub_query3 = $db->query("SELECT * FROM subjects WHERE id='$subject' ");
                    while($row=$sub_query3->fetch_assoc()){    
                      $sub_name=$row['name'];  
                    } 
              ?>
              <tr>
                <td><?php echo $ass_name ?></td>
                <td><?php echo $question ?></td>
                <td><?php echo $sub_name ?></td>
                <td><?php echo $class_name ?></td>
                <td> <a href="<?php echo $file_path ?>"> File </a>  </td>
                <td><?php echo $dueDate ?></td>
                <td><?php echo $assDate ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>  
      </div>

      <!-- Second Tab - Submmissions -->
      <div id="submissions" class="col s12 m8" >
        <div class="card-panel blue">
          <span class="white-text"> Recently Recieved Assignments </span>
        </div><br>
        <div class="row">
          <table class="responsive-table striped">
            <thead>
              <tr>
                <th>Student Name</th>
                <th>Date Submmited</th>
                <th>Late</th>
                <th>File</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $query = $db->query("SELECT * FROM assignments WHERE
                               question_id = '$question_id' && subject_id = '$subject_id' ")
                              or die("An error occured: ".mysqli_error($db));

                while($row=$query->fetch_assoc()){ 
                  $student_id = $row['student_id'];
                  $date_submmited = $row['date'];
                  $late = $row['late'];
                  $file = $row['assFile'];

                  $file_path = "../files/ass_notice/".$file; /**File location */

                  $stud_query = $db->query("SELECT * FROM students WHERE id='$student_id' ");
                  while($stud_row = $stud_query->fetch_assoc()){    
                    $student_name = $stud_row['name'];  
                  } 
                  $sub_query3 = $db->query("SELECT * FROM subjects WHERE id='$subject' ");
                  while($row=$sub_query3->fetch_assoc()){    
                    $sub_name = $row['name'];  
                  } 
                ?>
              <tr>
                <td><?php echo $student_name ?></td>
                <td><?php echo $date_submmited ?></td>
                <td><?php echo $late ?></td>
                <td> <a href="<?php echo $file_path ?>"> File </a>  </td>
                <td><a class="btn blue waves-effect waves-light" 
                  href="grade_ass.php?ass_id=<?php echo $ass_id."&class_id=".$class_id."&sub_id=".$subject_id."&stud_id=".$student_id; ?>">
                  Grade<i class="material-icons right">grade</i></a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
       </div>  
      </div>

      <!-- Second Tab - Graded -->
      <div id="graded" class="col s12 m8" >
        <div class="card-panel blue">
          <span class="white-text"> Graded Assignments </span>
        </div><br>
        <div class="row">
          <table class="responsive-table striped">
            <thead>
              <tr>
                <th>Student Name</th>
                <th>Marks</th>
                <th>Comment</th>
                <th>Date Graded</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $query = $db->query("SELECT * FROM results WHERE
                               name = '$ass_name' && subject_id = '$subject_id' ")
                              or die("An error occured: ".mysqli_error($db));
                while($row=$query->fetch_assoc()){ 
                  $student_id = $row['student_id'];
                  $marks = $row['marks'];
                  $commemt = $row['comment'];
                  $date = $row['date'];

                  $stud_query = $db->query("SELECT * FROM students WHERE id='$student_id' ");
                  while($stud_row = $stud_query->fetch_assoc()){    
                    $student_name = $stud_row['name'];  
                  }  
                ?>
              <tr>
                <td><?php echo $student_name ?></td>
                <td><?php echo $marks ?></td>
                <td><?php echo $commemt ?></td>
                <td> <?php echo $date ?> </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
       </div>  
      </div>

    <!-- column starts here -->
      <div class="col s12 m2">

      </div>
    <!-- column starts here   -->

</div>



    <?php ; ?>

    <?php require '../includes/footer.php'; ?>

  <!--  Scripts-->

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

  <!-- <script src="../js/materialize.js"></script> -->

  <script src="../js/init.js"></script>

  <script src="../js/script.js"></script>

</body>

</html>


<?php  ?>