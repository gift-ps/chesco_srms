<?php
	require_once('header.php');
?>

<body>

	<?php require '../includes/profile_navbar.php'; ?>

<div class="container">
	<div class="row">

		<!-- Notice starts here-->

	    <div class="col s12 m6">
		    <div class="card">

		        <div class="card-content">
					<span class="card-title">Send Notices</span>
					<?php require '../includes/notice_validation.php' ?>

		          	<form method="post" enctype="multipart/form-data">
						<div class="row">			    
							<div class="input-field col s12">
								<textarea id="textarea" class="materialize-textarea" name="name"></textarea>
								<label for="textarea">Enter your notice here</label>
							</div>

							<!-- file input starts here -->
							<div class="file-field input-field col s12">
								<div class="btn">
									<span>File</span>
									<input type="file" name="nFile" >
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text">
								</div>
							</div>
							<!-- file input ends here -->

							<div class="input-field col s12">
								<select name="class">
									<option value="" disabled selected>Choose Class</option>
									<?php 
									$get_class=$db->query("SELECT * FROM teacher_subject_class WHERE teacher_id = '$t_id' ");
									while($row1=$get_class->fetch_assoc()){
										$class_id = $row1['class_id'];

										$class_query=$db->query("SELECT * FROM classes WHERE id = '$class_id' ");
										while($row_sub = $class_query->fetch_assoc()){
											$class_id = $row_sub['id'];
											$class_name=$row_sub['name'];
									?>
										<option value="<?php echo $class_id ?>"><?php echo $class_name ?></option>
									<?php
										}
									}
									?>
								</select>
							</div>		      
						</div>

						<div class="card-action">
							<button class="btn blue waves-effect waves-light" type="submit" name="submit_notice">Send
							<i class="material-icons right">send</i>
							</button>
						</div>
		        	</form>

				</div>
		    </div>

	    </div>


	    <div class="col s12 m6">
			<div class="card-panel">
			<span class="bold">Past Notices </span>
			</div>
				<?php
					$sub_query = $db->query("SELECT * FROM notices WHERE teacher_id='$t_id' ORDER BY date");
					while($row1=$sub_query->fetch_assoc()){ 
						$notice = $row1['name'];
						$date = $row1['date'];
						$class = $row1['class'];

						$class_query = $db->query("SELECT * FROM classes WHERE id='$class' ");
						while($row_class=$class_query->fetch_assoc()){ 
							$class_name = $row_class['name'];
						}
				?>
				<div class="col s12 m12">
		          <div class="card">
		            <div class="card-content">
		              <span class="card-title left-align"><?php echo $t_username ?></span><p class="right-align"><?php echo $date ?><p>
					  <br> Class = <?php echo $class_name; ?><hr>
		              <p><?php echo $notice;?></p>
		            </div>
		          </div>
		    	</div>

		    	<?php }?>
		</div>

		<!-- Notice ends here -->

  	</div>
</div>



	<?php 
	;
	require '../includes/footer.php'; ?>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

  <!-- <script src="../js/materialize.js"></script> -->

  <script src="../js/init.js"></script>
  <script src="../js/script.js"></script>
</body>

</html>



<?php ?>