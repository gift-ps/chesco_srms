<?php
	require_once('header.php');
?>

<body>

<?php require '../includes/profile_navbar.php'; ?>
<div class="row">
    <div class="col s12 m1">
      <div class="card horizontal">
        <div class="card-stacked">                
            

           
        </div>
      </div>
    </div>
    
    <div class="row col s12 m10" style="margin-top: 10px;">
      <div class="col s12">
        <ul class="tabs blue-text">
          <li class="tab col s3"><a href="#pracs" class="active">New Assignments</a></li>
          <li class="tab col s3"><a href="#assgs">Past Assignments</a></li>
        </ul>
      </div>
      <div id="pracs" class="col s12">
        <div class="col s12 m12">
          <div class="card-panel blue">
            <span class="white-text">
                New Assignments
            </span>
          </div>
          <table class="striped highlight responsive-table">
            <thead>
              <tr>
                <th data-field="ass_no">Name</th>
                <th class="txt_limit" data-field="q">Queston</th>
                <th data-field="subject">Subject</th>
                <th data-field="class">Class</th>
                <th data-field="file">File</th>
                <th data-field="final_daet">Date Due </th>
                <th data-field="date">Date Created</th>
              </tr>
            </thead>

            <tbody>
                <?php
                    $query = $db->query("SELECT * FROM ass_notice WHERE class_id = '$class' ")
                              or die("Error: ".mysqli_error($db));

                    while($row=$query->fetch_assoc()){ 
                      $name = $row['name'];
                      $question = $row['question'];
                      $subject=$row['subject_id'];
                      $class=$row['class_id'];
                      $file=$row['assFile'];
                      $dueDate=$row['date_due'];
                      $assDate=$row['date'];

                      $file_path = "../files/ass_notice/".$file; /**File location */

                      $sub_query2 = $db->query("SELECT * FROM classes WHERE id='$class' ");
                      while($row2=$sub_query2->fetch_assoc()){    
                        $class_name=$row2['name'];  
                      } 
                      $sub_query3 = $db->query("SELECT * FROM subjects WHERE id='$subject' ");
                      while($row3=$sub_query3->fetch_assoc()){    
                        $sub_name=$row3['name'];  
                      } 
                ?>
                      <tr>
                          <td><?php echo $name ?></td>
                          <td><?php echo $question ?></td>
                          <td><?php echo $sub_name ?></td>
                          <td><?php echo $class_name ?></td>
                          <td> <a href="<?php echo $file_path ?>"> File </a>  </td>
                          <td><?php echo $dueDate ?></td>
                          <td><?php echo $assDate ?></td>
                      </tr>

                  <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div id="assgs" class="col s12">
        <div class="col s12 m12">
          <div class="card-panel blue">
            <span class="white-text">
            Assignments
            </span>
          </div>
          <table class="striped highlight responsive-table">
            <thead>
              <tr>
                  <th data-field="name">Name</th>
                  <th class="txt_limit" data-field="q">Question</th>
                  <th data-field="subject">Subject</th>
                  <th data-field="date">File</th>
                  <th>Date Submitted</th>
                  <th>Late/Intime</th>
                  <th>Marks</th>
                  <th>Comments</th>
              </tr>
            </thead>

            <tbody>

            <?php $sub_query = $db->query("SELECT * FROM assignments WHERE student_id='$id' ");
                while($row=$sub_query->fetch_assoc()){
                $q_id = $row['question_id'];
                $assFile = $row['assFile'];
                $date = $row['date'];
                $marks = $row['marks'];
                $late = $row['late'];
                $comment = $row['comment'];
                $my_file_path = "../files/assignment/".$assFile; /**File location */

                $ass_q_query = $db->query("SELECT * FROM ass_notice WHERE id = '$q_id'");
                while($ass_q_row = $ass_q_query->fetch_assoc()){ 
                  $name = $ass_q_row['name'];
                  $question = $ass_q_row['question'];

                  $sub_query3 = $db->query("SELECT * FROM subjects WHERE id='$subject' ");
                  while($row=$sub_query3->fetch_assoc()){    
                    $sub_name=$row['name'];  
                  }
                ?>
            
              <tr>
                <td><?php echo $name ?></td>
                <td><?php echo $question ?></td>
                <td><?php echo $sub_name ?></td>
                <th> <a href="<?php echo $my_file_path ?>"> File </a> </th>
                <td><?php echo $date ?></td>
                <?php
                  if(!empty($late)){?>
                    <td><font style="color: red;"><?php echo $late; ?></font></td>
                <?php }else {
                   echo "<th> In time</td>"; 
                  }
                ?>
                <td><?php if($marks == '1'){}else{echo $marks;} ?></td>
                <td><?php if (isset($comments)){echo $comments;} else{echo 'No comments yet';} ?></td>
              </tr>

              <?php }
               }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

</div>
    <?php require '../includes/footer.php'; ?>
    <?php ; ?>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>  
  <!-- <script src="../js/materialize.min.js"></script> -->
  <script src="../js/init.js"></script>
  <script src="../js/script.js"></script>
</body>
</html>

<?php  ?>