<?php
session_start();

include ('../assets/vars.php');
include '../assets/config.php';
include('../assets/auth.php');

$username = $_SESSION['username'];

$query = $_GET['query'];

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Results - <?php echo "$query"; ?></title>

  <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <link href="../assets/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <div id="wrapper">

    <!-- Sidebar -->
    <?php// include('parentSidebar.php') ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
          <?php include('../assets/topbar.php') ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Feeds Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h5 class="h5 mb-0 text-gray-800">Results for: <?php echo "$query"; ?> </h5>
            <button class="btn btn-sm btn-primary float-left" onClick="window.print()">Print this page</button>

          </div>
                    <hr>

           <?php 

            if($query){
                 
                $query = htmlspecialchars($query); 
                // changes characters used in html to their equivalents, for example: < to &gt;

                $r = "SELECT * FROM cases WHERE (`studentname` LIKE '%".$query."%') OR (`symptoms` LIKE '%".$query."%') OR (`studentid` LIKE '%".$query."%') OR (`reporter` LIKE '%".$query."%')";
                
                $query = mysqli_real_escape_string($db, $query);
                // makes sure nobody uses SQL injection

                $results = mysqli_query($db, $r)or die('Select error: '.mysqli_error($db));

                if(mysqli_num_rows($results) > 0){
                    while($row = mysqli_fetch_array($results)){
                  //Change date format                                
                  $date = new DateTime($row['date']);
                  $formattedDate = date_format($date, 'd M, Y - H:i');
            ?>

          <!-- Content Row -->

                  <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-between">
                      <h5 class="mb-1"><b><?php echo $row['studentname']; ?>  - <?php echo $row['studentid']; ?></b></h5>
                    </div>
                    <p class="mb-1"><b>Symptoms shown:</b> <?php echo $row['symptoms']; ?></p>
                    <p class="mb-1"><b>Had a temp of:</b> <?php echo $row['temperature']; ?></p>
                    <p class="mb-1"><b>Reported by:</b> <?php echo $row['reporter']; ?></p>
                    <p class="mb-1"><b>Reported on:</b> <?php echo $formattedDate; ?></p>
                    <p class="mb-1"><b>Full report:</b> <?php echo $row['report']; ?></p>
                    <?php if(isset($row['issue'])) { ?>
                      <p class="mb-1"><b>Diagnosed with suspected:</b> <?php echo $row['issue']; ?></p>
                    <?php } ?>
                    <?php if(isset($row['treatment'])) { ?>
                      <p class="mb-1"><b>Was recommended:</b> <?php echo $row['treatment']; ?></p>
                    <?php } ?>

                  </div>

          <?php

                      }

                       
                  }
                  else{ // if there is no matching rows report case to nurses
                      echo "Nothing found";
                  }
                   
              }
              else{ // if $symptoms variable is empty
                  echo "No records found";
              }
          ?>



        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer sidebar_new_bg">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Chengelo School 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src='https://kit.fontawesome.com/a076d05399.js'></script>

  <!-- Bootstrap core JavaScript-->
  <script src="../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../assets/sb-admin-2.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>
